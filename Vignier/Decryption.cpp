#include "Decryption.h"

void Decryption::KeyIntroduction()
{
	std::cout << "Please enter key: ";
	std::cin >> key;
	std::cout << "\n";
	std::cout << "Please enter the message: ";
	std::cin >> message;
	transform(message.begin(), message.end(), message.begin(), ::toupper);
	CreateKey();

}


void Decryption::CreateKey()
{
	EncryptedCharacter* encryptedKey = new EncryptedCharacter[key.size()];

	for (int index = 0; index < key.size(); index++)
	{
		(encryptedKey + index)->poz = initKey(key[index]);
		(encryptedKey + index)->character = key[index];
	}
	EncryptMessage(encryptedKey);

	delete[] encryptedKey;

}

void Decryption::EncryptMessage(EncryptedCharacter* encryptedKey)
{
	int counter = 0;
	for (int index = 0; index < message.size(); ++index)
	{
		if (message[index] - (encryptedKey + counter)->poz >= 65)
			message[index] = message[index] - (encryptedKey + counter)->poz;
		else
			message[index] = (message[index] - (encryptedKey + counter)->poz) + 26;
		counter++;
		if (counter == key.size())
			counter = 0;
	}

	Print(encryptedKey, message);

}


void Decryption::Print(EncryptedCharacter* encryptedKey, std::string message)
{
	for (int index = 0; index < key.size(); ++index)
	{
		std::cout << (char)(encryptedKey + index)->character << " " << (encryptedKey + index)->poz << "\n";

	}

	std::cout << "Mesajul decriptat este: " << message;
	std::cout << "\n";

}

int Decryption::initKey(char character)
{
	char alphabet = 'a';

	for (int index = 0; index < 26; ++index)
	{

		if (character == alphabet)
			return (index + 1);

		alphabet++;
	}
}