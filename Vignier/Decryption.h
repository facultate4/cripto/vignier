#pragma once
#include <string>
#include <iostream>
#include <algorithm>
class Decryption
{


public:
	std::string key;
	std::string message;

	struct EncryptedCharacter {
		char character;
		int poz;
	};

	void KeyIntroduction();
	void CreateKey();
	void EncryptMessage(EncryptedCharacter* encryptedKey);
	void Print(EncryptedCharacter* encryptedKey, std::string message);


private:


	int initKey(char character);



};

