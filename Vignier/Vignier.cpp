

#include <iostream>
#include "Encription.h"
#include "Decryption.h"

void StartProgram();

int main()
{

    StartProgram();
   
}

void StartProgram()
{
    Encription alfa;
    Decryption beta;
    int option;
    std::cout << "Welcome to Vignier Cipher" << "\n";
    std::cout << "If you want to encode a message press 1"<<"\n";
    std::cout << "If you want to decode a message press 2"<<"\n";
    std::cin >> option;
    std::cout << "\n";

    switch (option)
    {
    case 1:
        alfa.KeyIntroduction();
        break;

    case 2:
        beta.KeyIntroduction();
        break;

    default:
        break;
    }
}
